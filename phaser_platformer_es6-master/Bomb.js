class Bomb extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y){
        super(scene, x, y, "bomb");
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        let self=this;
        setTimeout(function(){
            scene.flames.push(new Flame(self.scene, x-9, y-9, -30, 0));
            scene.flames.push(new Flame(self.scene, x-9, y-9, 30, 0));
            scene.flames.push(new Flame(self.scene, x-9, y-9, 0, -30));
            scene.flames.push(new Flame(self.scene, x-9, y-9, 0, 30));
            scene.flames.push(new Flame(self.scene, x-9, y-9, -30, -30));
            scene.flames.push(new Flame(self.scene, x-9, y-9, 30, 30));
            scene.flames.push(new Flame(self.scene, x-9, y-9, 30, -30));
            scene.flames.push(new Flame(self.scene, x-9, y-9, -30, 30));
            self.disableBody(true, true);
        }, 1000);
    }
}