/**
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene1 extends Phaser.Scene {
    constructor() {
        super();
        this.player = null;
        this.platforms = null;
        this.cursors = null;
        this.lastBomb = -1;
    }
    preload() {
        this.load.image('tiles', 'assets/bomb_party_v4.png');
        this.load.tilemapTiledJSON('map', 'assets/level1.json');
        this.load.spritesheet('Bomberman',
            'assets/main_spritesheet.png',
            { frameWidth: 16, frameHeight: 16 }
        );

        this.load.spritesheet('Enemy',
            'assets/enemy.png',
            { frameWidth: 16, frameHeight: 16 }
        );
        this.load.image('brick', 'assets/tile204.png');
        this.load.image('bomb', 'assets/bomb.png');
        this.load.image('flame', 'assets/flame.png');
        this.pathfinder = new EasyStar.js();
    }
    create() {
        let self = this;
        this.cameras.main.setBounds(0, 0, 800, 800);
        const map = this.make.tilemap({ key: 'map' });
        const tileset = map.addTilesetImage('bomb_party_v4', 'tiles');
        this.platforms = map.createStaticLayer('MapLayer', tileset, 0, 0);
        map.setCollisionByExclusion([18]);
        this.map=map;
        this.anims.create({
            key: 'up',
            frames: this.anims.generateFrameNumbers('Bomberman', { start: 4, end: 5 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'down',
            frames: this.anims.generateFrameNumbers('Bomberman', { start: 0, end: 1 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'side',
            frames: this.anims.generateFrameNumbers('Bomberman', { start: 2, end: 3 }),
            frameRate: 10,
            repeat: -1
        })
        this.bricks = map.createFromObjects('BrickLayer', 205, { key: 'brick' });
        this.bricks.forEach(brick => {
            self.physics.world.enable(brick);
            brick.body.setImmovable(true);
        });
        
        this.player = new Player(this, 110, 450);
        this.cameras.main.startFollow(this.player);
        this.flames = new Array();
        this.cursors = this.input.keyboard.createCursorKeys();
        this.physics.add.collider(this.platforms, this.player);
        this.physics.add.collider(this.bricks, this.player);
        this.physics.add.collider(this.bricks, this.flames, function (b, f) {
            b.destroy();
            f.disableBody(true, true);
        });

        this.enemy = new Enemy(this, 200, 300);
        this.physics.add.collider(this.enemy, this.platforms);
        this.physics.add.collider(this.enemy, this.bricks, function (e, b) {
            e.fire();
        });
        this.updateGridMemory();
        
        this.graphics = this.add.graphics();
        this.graphics.fillStyle(0xff0000);

        this.circles=null;
        this.searchPath();
        
    }

    updateGridMemory(){
        let self=this;
        this.brickGrid=[];
        this.grid = [];
        //first, refresh brick info
        for (let y = 0; y < this.map.height; y++) {
            var col = [];
            for (let x = 0; x < this.map.width; x++) {
                col.push(false);
            }
            this.brickGrid.push(col);
        }
        this.bricks.forEach(brick => {
            if(brick.body && brick.body.enable){
                self.brickGrid[(brick.y-8)/16][(brick.x-8)/16]=true;
            }
        });
        //second, update map
        this.grid = [];
        for (let y = 0; y < this.map.height; y++) {
            var col = [];
            for (let x = 0; x < this.map.width; x++) {
                if(this.brickGrid[y][x]){
                    col.push(205);
                    continue;
                }
                let _x=8+(x)*16;
                let _y=9+(y)*16;
                let tile =this.map.getTileAt(x, y, false, 'MapLayer');
                col.push(tile.index);
            }
            this.grid.push(col);
        }
        this.pathfinder.setGrid(this.grid);
        this.pathfinder.setAcceptableTiles([18]);
    }

    searchPath(){
        let self=this;
        this.graphics.clear();
        this.graphics.fillStyle(0xff0000);
        this.pathfinder.findPath(parseInt(this.enemy.x/16), parseInt(this.enemy.y/16), 
            parseInt(this.player.x/16), parseInt(this.player.y/16), function (path) {
            setTimeout(function(){
                self.updateGridMemory();
                self.searchPath();
            }, 2000);
            if (path === null) {
                console.warn("Path was not found.");
            } else {
                // console.log(path);
                self.enemy.targetPath=path;
                self.circles=[];
                for(let p of path){
                    var circle = new Phaser.Geom.Circle(p.x*16+8, p.y*16+8, 2);
                    self.graphics.fillCircleShape(circle);
                    self.circles.push(circle);
                }
            }
        });
        this.pathfinder.calculate();
    }

    update() {
        if (this.flames.length > 0 && !this.flames[0].body.enable) {
            this.flames.shift();
        }
        let current = Date.now();
        this.player.setVelocityX(0);
        this.player.setVelocityY(0);
        this.enemy.setVelocityX(0);
        this.enemy.setVelocityY(0);
        if (this.cursors.left.isDown) {
            this.player.flipX = true;
            this.player.setVelocityX(-60);
            this.player.anims.play('side', true);
        } else if (this.cursors.right.isDown) {
            this.player.flipX = false;
            this.player.setVelocityX(60);
            this.player.anims.play('side', true);
        } else if (this.cursors.up.isDown) {
            this.player.flipX = false;
            this.player.setVelocityY(-60);
            this.player.anims.play('up', true);
        } else if (this.cursors.down.isDown) {
            this.player.flipX = false;
            this.player.setVelocityY(60);
            this.player.anims.play('down', true);
        }
        if (this.cursors.space.isDown) {
            if ((current - this.lastBomb) > 1000) {
                this.lastBomb = current;
                let bomb = new Bomb(this, this.player.x + 9, this.player.y + 9);
            }
        }
        
        this.enemy.update();
    }
}